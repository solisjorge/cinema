import { Component, OnInit } from '@angular/core';
import {BaseModel} from '../../model/basemodel';
import {MovieModel} from '../../model/moviemodel';
import {Movie, BusEvent} from '../../model/dto';
import { DeviceDetectorService } from 'ngx-device-detector';
import {MovieService} from '../../services/movie.service';

@Component({
  selector: 'app-movielist',
  templateUrl: './movielist.component.html',
  styleUrls: ['./movielist.component.css']
})
/**
* List of movies as a result of a search
*/
export class MovielistComponent implements OnInit {
   public movies:Movie[];
   public isMobile:boolean = false;
   public total:number = 0;
   public currentPage:number = 1;
   public keyword:string; //for pagination
   subscription:any;

  constructor(private _baseModel:BaseModel, 
  			  private _movieModel:MovieModel,
  			  private _movieService:MovieService,
  			  private _deviceDetector:DeviceDetectorService) {

  		this.checkMobile();
  } 

  //adapt the list if is mobile
  checkMobile(){
  	var deviceInfo = this._deviceDetector.getDeviceInfo();
    this.isMobile = this._deviceDetector.isMobile();
  }

  ngOnInit(): void {
  	this.subscription = this._baseModel.docItem$.subscribe(
        item => this.changed(item as BusEvent)
    );
  }

  changed(item:BusEvent){  	
    if(item.name=="search") {
    	this.movies = item.value['Search'];
    	this.total = item.value['totalResults'];
    	this.currentPage = 1;
    	this.keyword = this._baseModel.keyword;
    }
  } 

   loadPage(page:number){
      this._movieService.loadPage(this.keyword, page)
  		.subscribe(result => this.onList(result));
    }
  onList(data:Object){
  	this.movies = data['Search'];
  }
  deleteFromList(id){
  	this._movieModel.removeFromFavoriteById(id);
  }


}
