import { Component, OnInit } from '@angular/core';
import { BaseModel } from '../../model/basemodel';
import { User } from '../../model/dto';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})

/**
* Simple top menu to move between search and favorite list
**/
export class NavigationComponent implements OnInit {
	public menuIsCollapsed:boolean = true;
	public showNav:boolean = true;
	public unreadMsg:number = 1;
	public height = '100px';
	public user:User;

  constructor(private _baseModel:BaseModel) { }

  ngOnInit(): void {
  	this.user = this._baseModel.user;
  }

public makeRoom(){
	this.height = (this.menuIsCollapsed)?'120px':'300px';
};
public logout(){
	
};

}
