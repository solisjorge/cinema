import { Component, OnInit } from '@angular/core';
import {Movie} from '../../model/dto';
import {MovieModel} from '../../model/moviemodel';
import { DeviceDetectorService } from 'ngx-device-detector';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.css']
})
/**
* List of movies added to favorite list
* Here you can see the list and delete from the list
**/
export class FavoritesComponent implements OnInit {
    public favorites:Movie[];
    public isMobile:boolean = false;
    
  constructor(private _movieModel:MovieModel, private _deviceDetector:DeviceDetectorService) {

      this.checkMobile();
  } 

  //adapt the list if is mobile
  checkMobile(){
    var deviceInfo = this._deviceDetector.getDeviceInfo();
    this.isMobile = this._deviceDetector.isMobile();
  }

  ngOnInit(): void {
  	this.favorites = this._movieModel.favorites;
  }

  deleteFromList(id){
  	if(confirm('Are you sure you want to delete this movie\nfrom your favorite list?')){
  		this._movieModel.removeFromFavoriteById(id);
  		this.favorites = this._movieModel.favorites;	
  	}
  	
  }
}
