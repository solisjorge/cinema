import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { BaseModel } from '../../model/basemodel';
import { User } from '../../model/dto';

/**
* Very simple guard to check
* if a user is logged
*/

@Injectable()
export class IsLogged implements CanActivate {

  private user: User

  constructor(
    private router: Router,
    private _baseModel: BaseModel
  ) {}

  canActivate() {
    this.user = this._baseModel.user
    if (!this.user) {
      // usuario no logueado
      this.router.navigate([''])
      return false
    }
    return true
  }
}