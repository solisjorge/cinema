import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

/**
* Main template holding the search and the listing
* Just a container
**/
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
