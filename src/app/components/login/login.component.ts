import { Component, OnInit } from '@angular/core';
import { BaseModel } from '../../model/basemodel';
import {User} from '../../model/dto';
import { UserService} from '../../services/user.service';
import {Router} from '@angular/router';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

/**
* Login form as the entry point
* service is simulated so any user/pass will success
**/
export class LoginComponent implements OnInit {

    public recovery = false;
    public email:string = '';
    public password:string = '';
    public loginerror:string = ''
    public showLogo:number=1;
    public askforpassword:string;
    public label:string = "Sign in";
    public isMobile:boolean = false;

    constructor(
        private _baseModel:BaseModel, 
  		  private _api:UserService, 
        private router:Router) {
    }

  ngOnInit() { 
    this._baseModel.cleanUser();
  }

  public loginUser(){
    if(this.email.length<3) return this.loginerror = 'Email is mandatory';
    if(this.label=="Send password") return this.sendPassword()
    if(this.password.length<3) return this.loginerror = 'Password is mandatory';
    this._api.loginUser(this.email, this.password)
        .subscribe(result=>this.onLogin(result))
  }

  sendPassword(){
    //JUST A SIMULATION
    this.onPassword({})
  }

  onPassword(data:object){
    this.loginerror = "";
    this.askforpassword = 'Password sent. Please reload the form to login';
  }

  private onLogin(data:object){   
    this._baseModel.user = data as User;
    //JUST SIMULATION, no error expected    
    //if(data['status']=="error") return this.loginerror = data['msg'];
    this.loginerror = "";
    this.router.navigate(['home']);
  }

  public askForPass(){
    this.recovery = true;
    this.askforpassword = "Please enter the account email";
    this.label = "Send password";
    this.loginerror = '';
  }

}
