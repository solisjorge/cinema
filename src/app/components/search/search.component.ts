import { Component, OnInit, Input, EventEmitter  } from '@angular/core';
import { MovieService } from '../../services/movie.service';
import { User } from '../../model/dto';
import { Router } from '@angular/router';
import { BaseModel } from '../../model/basemodel';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})

export class SearchComponent implements OnInit {

  public keyword:string = ' ';
  public gralerror:string;
  
  constructor(private _api:MovieService, private _baseModel:BaseModel) { 
  }

 ngOnInit() {}

  gralSearch(){
    this.gralerror = '';
    //console.log("Keyword: "+this.keyword)
    if(this.keyword==' ' || this.keyword.length<3) return this.gralerror ='Keyword should have at least 3 characters'
    this._api.searchMovieByTitle(this.keyword)
       .subscribe(result => this.onResult({data:result}))  
  }

  onResult(data:object){
    this._baseModel.keyword = this.keyword;
    this._baseModel.setProp({name:'search', value:data['data']})
  }
}
