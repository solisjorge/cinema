import { Component, OnInit } from '@angular/core';
import {MovieModel} from '../../model/moviemodel';
import {MovieService} from '../../services/movie.service';
import {Movie} from '../../model/dto';
import { Router, ActivatedRoute } from '@angular/router';
import { DeviceDetectorService } from 'ngx-device-detector';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css'],
  inputs:['movie']
})

/**
* Details for a movie selected in the movielist
* An api call is made at beginning using the id
* passed on the url
**/
export class DetailComponent implements OnInit {
	public movie:Movie;
  subscription:any;
  protected id:string;
  public isMobile:boolean = false;

  constructor(private _movieModel:MovieModel, 
              private _movieService:MovieService,
              private route:ActivatedRoute,
              private _deviceDetector:DeviceDetectorService) {
    this.checkMobile();
  } 

  //adapt the list if is mobile
  checkMobile(){
    var deviceInfo = this._deviceDetector.getDeviceInfo();
    this.isMobile = this._deviceDetector.isMobile();
  }

  ngOnInit(): void {
    this.subscription = this.route.params.subscribe(params => {
         this.id = params['id'];
         //first check addToFavorites
         this.movie = this._movieModel.getFromFavoriteById(this.id);
         //if not there load from service
         if(!this.movie) this._movieService.getMovieById(this.id)
           .subscribe(result=>this.onMovie(result))        
     });
  }

  onMovie(data:Object){
    this.movie = data as Movie;
  }

  addToFavorites(){
    if(this._movieModel.checkIsFavorite(this.movie)){
      this.movie.isFavorite = false;
      this._movieModel.removeFromFavorite(this.movie)
    } else {
      this.movie.isFavorite = true;
      this._movieModel.addToFavorite(this.movie);
    }
  }

}
