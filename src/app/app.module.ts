import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { HttpClientModule,HTTP_INTERCEPTORS ,HttpClient,HttpHandler } from '@angular/common/http';
import { UserInterceptor } from './services/user.interceptor';
import { UserService } from './services/user.service';
import { SearchComponent } from './components/search/search.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { DetailComponent } from './components/detail/detail.component';
import { FavoritesComponent } from './components/favorites/favorites.component';
import { MovielistComponent } from './components/movielist/movielist.component';
import { DeviceDetectorModule } from 'ngx-device-detector';
import { IsLogged } from './components/guard/islogged';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    SearchComponent,
    NavigationComponent,
    DetailComponent,
    FavoritesComponent,
    MovielistComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    DeviceDetectorModule 
  ],
  providers: [{
      provide: HTTP_INTERCEPTORS,
      useClass: UserInterceptor,
      multi: true
    }, HttpClient, UserService,IsLogged ],
  bootstrap: [AppComponent]
})
export class AppModule { }
