import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import {HomeComponent} from './components/home/home.component';
import { FavoritesComponent } from './components/favorites/favorites.component';
import {DetailComponent} from './components/detail/detail.component';
import {IsLogged} from './components/guard/islogged';

const routes: Routes = [
	{ path: '', component: LoginComponent },
	{ path: 'login', component: LoginComponent },
	{ path: 'home', component: HomeComponent, canActivate:[IsLogged]},
	{ path: 'detail/:id', component: DetailComponent, canActivate:[IsLogged] },
	{ path: 'favorites', component: FavoritesComponent, canActivate:[IsLogged] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
