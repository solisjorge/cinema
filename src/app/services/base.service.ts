import { Injectable, ReflectiveInjector, Injector, OnInit } from '@angular/core';
import { HttpClient, HttpXhrBackend, HttpHandler, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, asapScheduler, pipe, of, from, interval, merge, fromEvent, throwError } from 'rxjs';
import { map, filter, scan, catchError, mergeMap, finalize } from 'rxjs/operators';

/**
* BaseService: here we can add whatever we need
* for the whole services. Right now only handling
* preload feedback. Typical task: authentication
* cache, special headers (but you can do the same with interceptors though)
*/
@Injectable({
  providedIn: 'root'
})
export class BaseService {
  private loadQueue:number = 0;
  private timeout:number = 30000

  constructor(protected http:HttpClient) {
  }

  
  get(url:string):Observable<any> {
    if(this.loadQueue==0) this.startLoading();
    var pending:Observable<any> = this.http.get(url).pipe(finalize(() => this.finish()));
    return pending;
  }

  startLoading(){
    if(this.loadQueue>=1) return      
    var el = window.document.getElementById("loader")
    if(el) el.className = "c-loader is-visible";    
    this.loadQueue++;
    setTimeout(function(w){ w.finish() }, this.timeout, this); //30 sgs
  }
  
  finish(){
    this.loadQueue--;    
    if(this.loadQueue<=0){      
      var el = window.document.getElementById("loader")
      if(el) el.classList.remove('is-visible');       
      this.loadQueue = 0;
    } 
  }

  //General error handling
  protected handleError(error: any, caught: Observable<Object[]>) {
    try{    
        return Observable.throw(error || 'Server error');
    } catch(e){        
        console.log(error);
        alert('Problem loading the service, please try again later !')
    }
  }
}