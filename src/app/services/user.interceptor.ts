import { Injectable, Injector } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse,  HttpHandler, HttpHeaders, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import {User} from '../model/dto';

@Injectable()
/**
* Simulate login
**/
export class UserInterceptor implements HttpInterceptor {
 private fakeUser:User;

 constructor(private injector: Injector) {
   this.fakeUser = new User(1, 'solisarg', 'secreto', 'solisarg@gmail.com')
 }

 intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
 	 //console.log('interceptor de '+request.url)
     if(request.method === "POST" && request.url === "http://localhost/api/loginuser") {
          return of(new HttpResponse({ status: 200, body: this.fakeUser }));
     }
     return next.handle(request)
 }
}