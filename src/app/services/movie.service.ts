import { Injectable } from '@angular/core';
import { map, catchError } from 'rxjs/operators';
import { Observable, pipe } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { BaseService } from '../services/base.service';

@Injectable({
  providedIn: 'root'
})
/**
* Calls to the OMDB API
**/
export class MovieService extends BaseService {

  //this is usually in an environment file
  private domain:string = "http://www.omdbapi.com/?apikey=f12ba140"

  constructor(private _http:HttpClient) {
  	super(_http);
  }

  public searchMovieByTitle(title:string):Observable<any>{
      return this.get(this.domain+'&s='+title.trim()).pipe(          
        catchError(this.handleError)
      ); 
  }

  public getMovieById(id:string){
      return this.get(this.domain+'&plot=full&i='+id.trim()).pipe(          
          catchError(this.handleError)
        ); 
  }

  public loadPage(key:string, page:number){
      return this.get(this.domain+'&s='+key.trim()+'&page='+page).pipe(          
          catchError(this.handleError)
        ); 
  }
}
