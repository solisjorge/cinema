import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, filter, scan, catchError, mergeMap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { BaseService } from '../services/base.service';
import { User } from '../model/dto';

@Injectable({
  providedIn: 'root'
})
/**
* This class is never used, the interceptor
* catch the login, anyway leave here for completeness
**/
export class UserService extends BaseService {

	private domain:string = 'http://localhost/';

    constructor(private _http:HttpClient) {
        super(_http);
    }

    public loginUser(email:string, pass:string):Observable<any>{
	    let body = JSON.stringify({email:email,password:pass});
	    return this._http.post(this.domain+'api/loginuser', body).pipe(
	       catchError(this.handleError)
	     ); 
	  }
}
