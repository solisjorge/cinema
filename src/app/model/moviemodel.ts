import * as Rx from "rxjs";
import { Injectable } from '@angular/core';
import {BaseModel} from '../model/basemodel';
import { User,Movie } from '../model/dto';

@Injectable({
  providedIn: 'root'
})
export class MovieModel extends BaseModel {

	private _favorites:Movie[];

	constructor(){
		super();
		
		var val = localStorage.getItem('favoritos');
		if(val)
			this._favorites = JSON.parse(val);
		else
			this._favorites = [];
	}

	public addToFavorite(data:Movie){
		var movie:Movie = this._favorites.filter(item => item.imdbID == data.imdbID)[0];
		this._favorites.push(data);
		localStorage.setItem('favoritos', JSON.stringify(this._favorites));
	}

	public removeFromFavorite(data:Movie){
		this._favorites = this._favorites.filter(function(item) {
		    return item.imdbID != data.imdbID;
		});
		localStorage.setItem('favoritos', JSON.stringify(this._favorites));
	}

	public getFromFavoriteById(id){
		var movie:Movie = this._favorites.filter(item => item.imdbID == id)[0];
		return movie;
	}

	public removeFromFavoriteById(id){
		var movie:Movie = this._favorites.filter(item => item.imdbID == id)[0];
		if(movie) this.removeFromFavorite(movie);
	}

	public checkIsFavorite(data:Movie):boolean{
		var movies:Movie[] = this._favorites.filter(item => item.imdbID == data.imdbID);
		return (movies.length > 0);
	}

	public get favorites():Movie[]{
		return this._favorites;
	}

}