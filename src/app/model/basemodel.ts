import * as Rx from "rxjs";
import { Injectable } from '@angular/core';
import { User,Movie } from '../model/dto';

/********************************************
* Base Model holds all common methods       *
* notabily the data bus as asObservable     *
* Also store                                *
********************************************/
@Injectable({
  providedIn: 'root'
})
export class BaseModel {
	
	private doc = new Rx.BehaviorSubject({});
	public docItem$ = this.doc.asObservable();
	private _user:User;
	private _keyword:string;

	constructor() {}
	
	setProp(data: Object) {
		console.log("MODEL " + data["name"] + "=" + data["value"]);
		this[data["name"]] = data["value"];
		this.doc.next(data);
	}

	//store current user
	public set user(data:User){
		this._user = data;
		//persist
		var datos = JSON.stringify(data)				
		sessionStorage.setItem('User', datos);
	}

	public get user():User{
		var data = sessionStorage.getItem('User');
	    return (data)?JSON.parse(data):null;	    	    
	}

	public set keyword(k:string){
		this._keyword = k;
	}

	public get keyword():string{
		return this._keyword;
	}

	public cleanUser(){
		this._user = null;
		sessionStorage.setItem('User', null);
	}
}