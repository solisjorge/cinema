# README #

This is a simple application where you can search movies, developed using Angular 9

### How do I get set up? ###

* Fork this repo
* If you don't have Node installed, install Node.js
* Once you have Node installed, run 'npm install' to download all the dependencies
* When download is finished, prompt into the folder and run 'ng serve'
* Open your browser at http://localhost:4200
* Use any email and password to login, it's a simulation

### Demo ###
You can see the working app at http://devarg3818.com/movies/

### Who do I talk to? ###

* If you have any problem contact the owner at solisarg@gmail.com